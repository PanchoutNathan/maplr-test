import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { TeamsService } from './teams.service';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { UpdatePlayerDto } from '../players/dto/update-player.dto';
import { CreatePlayerDto } from '../players/dto/create-player.dto';

@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}

  @Post()
  create(@Body() createTeamDto: CreateTeamDto) {
    return this.teamsService.create(createTeamDto);
  }

  @Get(':year/captain')
  getCaptain(@Param('year') year: string) {
    return this.teamsService.getCurrentCaptain(+year);
  }

  @Get('import')
  importAllTeamFromJson() {
    return this.teamsService.importAllTeamsFromData();
  }

  @Get()
  findAll() {
    return this.teamsService.findAll();
  }

  @Post(':year')
  addPlayer(@Body() player: CreatePlayerDto, @Param('year') year: string) {
    return this.teamsService.addPlayer(+year, player);
  }

  @Get(':year')
  findByYear(@Param('year') year: string) {
    return this.teamsService.findByYear(+year);
  }
}
