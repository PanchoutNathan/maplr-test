import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Player } from '../../players/entities/player.entity';

@Entity('teams')
export class Team {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column('text', { nullable: false })
  public coach: string;

  @Column('int', { nullable: false, unique: true })
  public year: number;

  @ManyToMany(() => Player, (player) => player.team, {
    cascade: true,
  })
  @JoinTable()
  public players: Player[];
}
