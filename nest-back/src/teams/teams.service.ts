import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Team } from './entities/team.entity';
import { getConnection, Repository } from 'typeorm';
import * as data from './../data/data.json';
import { Player } from '../players/entities/player.entity';
import { CreatePlayerDto } from '../players/dto/create-player.dto';
import { PlayersService } from '../players/players.service';

@Injectable()
export class TeamsService {
  constructor(
    @InjectRepository(Team)
    private teamsRepository: Repository<Team>,
    @Inject(forwardRef(() => PlayersService))
    private playersService: PlayersService,
  ) {}

  /**
   * @param createTeamDto
   */
  async create(createTeamDto: CreateTeamDto) {
    const newTeam = await this.teamsRepository.create(createTeamDto);
    await this.teamsRepository.save(newTeam);
    return newTeam;
  }

  findAll() {
    return this.teamsRepository.find();
  }

  /**
   * @param year
   */
  findByYear(year: number) {
    return this.teamsRepository.findOne({ year }, { relations: ['players'] });
  }

  /**
   * @param year
   * @return Promise<Player | null>
   */
  async getCurrentCaptain(year: number): Promise<Player | null> {
    return getConnection()
      .getRepository(Player)
      .createQueryBuilder('player')
      .leftJoin('player.team', 'team')
      .where('player.is_captain IS TRUE')
      .andWhere('team.year = :year', { year: year })
      .getOne();
  }

  /**
   * @param year
   * @param player
   */
  async addPlayer(year: number, player: CreatePlayerDto) {
    const team = await this.findByYear(year);
    team.players.push(player);
    if (player.is_captain) {
      const currentCaptain = await this.getCurrentCaptain(year);
      this.playersService.unmakeCaptain(currentCaptain);
    }
    await this.teamsRepository.save(team);
    return player;
  }

  /**
   * @return Promise<void>
   */
  async importAllTeamsFromData(): Promise<void> {
    let lastTeam: any = data[0];
    const teams: any[] = [];
    data.forEach((team) => {
      if (lastTeam.id === team.id) {
        lastTeam = team;
        return;
      }
      teams.push(lastTeam);
      lastTeam = team;
    });
    teams.push(data[data.length - 1]);

    for (const team of teams) {
      await this.create(team);
    }
  }
}
