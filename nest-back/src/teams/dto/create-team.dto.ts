import { Player } from '../../players/entities/player.entity';

export class CreateTeamDto {
  year: number;
  coach: string;
  players?: Player[];
}
