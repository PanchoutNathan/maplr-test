import { Controller, Param, Put } from '@nestjs/common';
import { PlayersService } from './players.service';

@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}

  @Put(':id/captain')
  async makeCaptain(@Param('id') id: string) {
    const player = await this.playersService.findOne(+id);
    return this.playersService.makeCaptainById(+id, {
      ...player,
      is_captain: true,
    });
  }
}
