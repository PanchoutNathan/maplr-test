import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { UpdatePlayerDto } from './dto/update-player.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Team } from '../teams/entities/team.entity';
import { getConnection, Repository, UpdateResult } from 'typeorm';
import { Player } from './entities/player.entity';
import { TeamsService } from '../teams/teams.service';

@Injectable()
export class PlayersService {
  constructor(
    @InjectRepository(Player)
    private playerRepository: Repository<Player>,
    @Inject(forwardRef(() => TeamsService))
    private teamService: TeamsService,
  ) {}

  /**
   * @param id
   * @return Promise<Player>
   */
  findOne(id: number): Promise<Player> {
    return this.playerRepository.findOne({ id });
  }

  /**
   * @param id
   * @return Promise<Team | null>
   */
  async getTeam(id: number): Promise<Team | null> {
    return await getConnection()
      .getRepository(Team)
      .createQueryBuilder('team')
      .leftJoin('team.players', 'players')
      .where('players.id = :id', { id: id })
      .getOne();
  }

  /**
   * @param id
   * @param updatePlayerDto
   * @return Promise<Result>
   */
  async update(
    id: number,
    updatePlayerDto: UpdatePlayerDto,
  ): Promise<UpdateResult> {
    return this.playerRepository.update(id, updatePlayerDto);
  }

  /**
   * @param player
   * @return Promise<Result>
   */
  async unmakeCaptain(player?: Player): Promise<UpdateResult> {
    if (player == null) {
      return;
    }
    return this.update(player.id, {
      ...player,
      is_captain: false,
    });
  }

  /**
   * @param id
   * @param updatePlayerDto
   */
  async makeCaptainById(id: number, updatePlayerDto: UpdatePlayerDto) {
    const team = await this.getTeam(id);
    const currentCaptain = await this.teamService.getCurrentCaptain(team.year);
    if (currentCaptain) {
      await this.unmakeCaptain(currentCaptain);
    }
    await this.playerRepository.update(id, updatePlayerDto);
    return updatePlayerDto;
  }
}
