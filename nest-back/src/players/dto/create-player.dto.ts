export class CreatePlayerDto {
  name: string;
  lastname: string;
  position: string;
  number: number;
  is_captain: boolean;
}
