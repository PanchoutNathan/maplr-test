import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Team } from '../../teams/entities/team.entity';

@Entity('players')
export class Player {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column('int', { nullable: true, default: null })
  public number: number;

  @Column('text', { nullable: true, default: null })
  public name: string;

  @Column('text', { nullable: true, default: null })
  public lastname: string;

  @Column('text', { nullable: true, default: null })
  public position: string;

  @Column('boolean', { nullable: false, default: false })
  public is_captain: boolean;

  @ManyToMany(() => Team, (team) => team.players)
  public team?: Team;
}
