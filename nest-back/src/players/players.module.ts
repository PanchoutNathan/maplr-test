import { forwardRef, Module } from '@nestjs/common';
import { PlayersService } from './players.service';
import { PlayersController } from './players.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Player } from './entities/player.entity';
import { TeamsModule } from '../teams/teams.module';
import {TeamsService} from "../teams/teams.service";

@Module({
  imports: [TypeOrmModule.forFeature([Player]), forwardRef(() => TeamsModule)],
  controllers: [PlayersController],
  providers: [PlayersService],
  exports: [PlayersService],
})
export class PlayersModule {}
