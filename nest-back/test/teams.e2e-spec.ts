import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as supertest from 'supertest';
import { getConnectionOptions, Repository } from 'typeorm';
import { Player } from '../src/players/entities/player.entity';
import { PlayersModule } from '../src/players/players.module';
import { Team } from '../src/teams/entities/team.entity';
import { TeamsModule } from '../src/teams/teams.module';
import { TestDatabaseModule } from '../src/test-database/test-database.module';

describe('User', () => {
  let app: INestApplication;
  let repository: Repository<Team>;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [TeamsModule, TestDatabaseModule],
    }).compile();
    //
    app = module.createNestApplication();
    repository = module.get('TeamRepository');
    await app.init();
  });

  afterEach(async () => {
    await repository.query(`DELETE FROM teams;`);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('GET /teams', () => {
    it('should return an array of users', async () => {
      expect(true).toEqual(true);
      await repository.save([
        {
          coach: 'Nathan',
          year: 2010,
          players: [
            {
              name: 'Owen',
              lastname: 'McDonald',
              number: 10,
              position: 'Defenser',
              is_captain: false,
            },
          ],
        },
        { coach: 'Nathan', year: 2011 },
      ]);
      const { body } = await supertest
        .agent(app.getHttpServer())
        .get('/teams')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);
      expect(body).toEqual([
        { id: expect.any(Number), coach: 'Nathan', year: 2010 },
        { id: expect.any(Number), coach: 'Nathan', year: 2011 },
      ]);

      const response = await supertest
        .agent(app.getHttpServer())
        .get('/teams/2010')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body).toEqual({
        id: expect.any(Number),
        coach: 'Nathan',
        year: 2010,
        players: [
          {
            id: expect.any(Number),
            name: 'Owen',
            lastname: 'McDonald',
            number: 10,
            position: 'Defenser',
            is_captain: false,
          },
        ],
      });
    });
  });
});
