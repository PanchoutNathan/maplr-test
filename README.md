# Maplr-test

## Stack ##
- Front: j'ai utilisé React comme demandé. Pour le projet, il y avait une option qui était d'utiliser un Framework UI. 
J'aurais pu utiliser Tailwind, mais j'ai décidé d'utiliser Material UI qui est un framework UI pour React que j'ai l'habitude 
d'utiliser. Je n'ai pas eu besoin d'utiliser un router. J'ai fait tout dans la même page.


- Back: NestJS avec TypeORM 


- Database : Postgres

J'ai mis entre 10h et 12h pour le faire (Hors préparation pour comprendre le jeu de données)

## Docker ## 

J'ai tout intégré dans Docker (React + Nest + Postgres, en mode développement). Comme ça vous avez qu'un `docker-compose up` à faire.

Le front se trouve sur le port 3001.
Le back se trouve sur le port 3000.

Une fois la stack docker mise en place, tapez `http://localhost:3000/api/teams/import` pour importer le jeu de donnée en base.
ATTENTION : ne faire qu'une seule fois. Il n'y pas de mécanisme de détection si les données sont déjà en base.

Puis rendez-vous sur `http://localhost:3001/` pour accéder au front.

### Remarque sur le mode production ### 
Je n'ai pas écrit de Dockerfile / docker-compose pour un mode production. Je pense que ce n'étais pas l'objectif. 
Cependant je peux vous décrire comme je fais pour déployer mon projet personnel qui est sur la même stack (sauf la databse qui est sur du MongoDB)
, tout est automatisé : 

- Je build mon react en mode production 
- Je copie tout le contenu du dossier du build dans le dossier public du nest (Nest peut rendre évidement du static)
- Pour finir je lance mon nest en mode production



## e2e Testing ## 
J'ai écrit des tests e2e avec une base de test. 
Je préfère cette approche car comme ça on est au plus prêt de l'utilisateur final.



Pour lancer les tests, à l'aide de votre terminal rendez-vous à la racine du projet `nest-back`. Puis tapez la commande : 
`npm run test:e2e`

## Remarques ##

### Jeu de données / schema.sql ### 

J'imagine que la duplication des équipes dans le data.json était voulu, mais je vous fait part de mes remarques  au cas ou :

- Pour une équipe, il y a autant d'objets qu'il y a de joueurs, avec l'attribut `players` qui croit au fur et à mesure (de taille 1, puis 2, ... 34).
- L'attribut coach est toujours vide. Je ne l'ai donc pas affiché sur le front.
- Attention, l'attribut pour le capitaine de l'équipe est définit `is_capitain` (au lieu de `is_captain`) avec le souhait de le marqué en Anglais 
au vu des autres attributs. Dans ma code base, j'ai mis `is_captain`.

#### Joueurs dupliqués #### 

J'ai rémarqué que dans le jeu de données il y avait des duplications pour les joueurs avec le même nom / prénom
(je suis parti du principe que c'était les mêmes joueurs), Un joueur avec le même nom / prénom peuvent avoir un id différent.

J'ai cru comprendre que c'était pour le `is_captain`. Mais ça peut posser des problèmes,  si on souhaite modifier
les informations d'un joueur en particulier, il faut le modifier partout par exemple. J'ai laissé comme l'implémentation était prévu mais une solution aurait été
d'ajouter un tableau par joueur pour savoir toutes les années ou il a été capitaine pour l'équipe.
Ou encore encore mieux de l'ajouter sur le liens car comme ça on pourrait avoir appartenu à plusieurs équipes et avoir était capitaine dans ces équipes. 
Ça évite la duplication et la place en BDD.
### Features ### 
J'ai eu un souci de compréhension dans le descriptif du projet : 

        Vous devez créer les endpoints suivants dans l'API:
           - Un POST pour t'ajouter en tant que nouveau joueur de l'équipe d'une année définie dans la base de données.


        Vous devez créer les fonctionnalités suivantes dans l'application web :
            - Possibilité d'ajouter un joueur dans l'équipe d'une année.

Entre le front et le back, la description de la feature n'est pas la même. Faut-il : 
- Pouvoir s'ajouter soi-même dans une équipe, 
- Ajouter un joueur qui existe déjà dans la base 
- Ajouter un joueur que tu renseigne avec des champs pour renseigner les informations.

J'ai envoyé un mail pour précision, mais je n'ai pas eu de réponse au bout de 2 jours donc j'ai opté pour la dernière qui était peut être la plus complète dans le doute. Ça m'a permis de vous montrer que j'utilise
Formik + Yup (les grands classiques) pour la gestion et la validation de formulaire.


### Routes API ### 
J'ai modifié légèrement les paths des routes pour que ça corresponde plus aux conventions de nommage des ressources REST

J'ai modifié de cette façon : 

    `GET /api/teams/{year}`

    `POST /api/teams/{year}`

    `PUT /api/players/{id}/captain`





#### Migration avec TYPEORM ####

La configuration de TypeOrm est pour un environnement de dev. En production, il ne faudrait surtout pas utiliser l'attribut
`synchronize: true`. Il faudrait donc créer des migrations pour un mode de production. 

