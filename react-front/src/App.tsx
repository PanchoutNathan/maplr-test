import React, {SyntheticEvent, useEffect, useState} from 'react';
import './App.scss';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarkBorderOutlinedIcon from '@mui/icons-material/BookmarkBorderOutlined';
import {
    AppBar,
    Autocomplete,
    Box,
    Button,
    Container,
    IconButton,
    TextField,
    Toolbar,
    Tooltip,
    Typography
} from "@mui/material";
import http from "./services/http/http.service";
import {AxiosError} from "axios";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import {AddPlayerModal} from "./components/AddPlayerModal";
import {PlayerEntity} from "./common/entities/player.entity";
import {TeamEntity} from "./common/entities/team.entity";

interface AppStates {
    teams: TeamEntity[];
    teamPlayers: PlayerEntity[];
    selectedTeam: TeamEntity | null;
    inputValue: string;
    isLoading: boolean;
    openAddPlayerModal: boolean;
}

function App() {
    const [state, setState] = useState<AppStates>({
        teams: [],
        selectedTeam: null,
        inputValue: '',
        teamPlayers: [],
        isLoading: false,
        openAddPlayerModal: false,
    })


    useEffect(() => {
        http.get('/teams').then((response) => {
            setState(prevState => ({...prevState, teams: response.data}))
        }).catch((error: AxiosError) => {
            console.log(error);
        })
    }, [])

    const handleChangeSelectedTeam = (team: any): void => {
        if (team == null) {
            setState(prevState => ({...prevState, selectedTeam: null, inputValue: '', teamPlayers: []}))
            return;
        }
        setState(prevState => ({...prevState, isLoading: true}));
        http.get(`/teams/${team.year}`).then((response) => {
            setState(prevState => (
                {
                    ...prevState,
                    isLoading: false,
                    selectedTeam: team,
                    teamPlayers: response.data.players
                }))
        }).catch((error: AxiosError) => {
            console.log(error);
        })
    }
    const handleMakeCaptain = (player: any): void => {
        setState(prevState => ({
            ...prevState, isLoading: true
        }));
        http.put(`/players/${player.id}/captain`).then((response) => {
            handleChangeSelectedTeam(state.selectedTeam);
        }).catch((error: AxiosError) => {
            console.log(error)
        })
    }

    const columns: GridColDef[] = [
        { field: 'name', headerName: 'Name', width: 150, editable: false},
        { field: 'lastname', headerName: 'Last name', width: 150 },
        { field: 'number', headerName: 'Number', width: 150 },
        { field: 'position', headerName: 'Position', width: 150 },
        { field: 'is_captain', headerName: 'Capitaine ?', width: 100},
        {field: '',  headerName: 'Action', width: 150, renderCell: (params) => {

                const player: any = params.row;
                return <Tooltip title={'Make capitain'}>
                    <IconButton onClick={() => handleMakeCaptain(player)} color="secondary" aria-label="add an alarm">
                        {player.is_captain && <BookmarkIcon />}
                        {!player.is_captain && <BookmarkBorderOutlinedIcon />}
                    </IconButton>
                </Tooltip>
            } },
    ];


    const handleOpenAddPlayerModal = (): void => {
        setState(prevState => ({...prevState, openAddPlayerModal: true}));
    }

    const handleCloseAddPlayerModal = (): void => {
        setState(prevState => ({...prevState, openAddPlayerModal: false}));
        handleChangeSelectedTeam(state.selectedTeam);
    }


    return <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Maplr test
                </Typography>

            </Toolbar>
        </AppBar>
        <Container sx={{padding: '20px 10px'}} maxWidth={'md'}>
            <Box display={'flex'} justifyContent={'space-between'}>

            <Autocomplete
                disablePortal
                fullWidth
                size={'small'}
                value={state.selectedTeam}
                onChange={(event: SyntheticEvent, newValue: TeamEntity | null) => {
                    handleChangeSelectedTeam(newValue);
                }}
                inputValue={state.inputValue}
                onInputChange={(event, newInputValue) => {
                    setState(prevState => ({...prevState, inputValue: newInputValue}));

                }}
                getOptionLabel={(option) => option.year + ''}
                options={state.teams}
                sx={{ maxWidth: 200, marginRight: '10px' }}
                renderInput={(params) => <TextField {...params} label="Année" />}
            />

            {state.selectedTeam && <Button variant={"outlined"} size={'small'} onClick={handleOpenAddPlayerModal}>+ joueur</Button>}
            </Box>

            <div style={{ height: '80vh', paddingTop: '10px', width: '100%' }}>
                <DataGrid loading={state.isLoading}  rows={state.teamPlayers} columns={columns} />
            </div>
        </Container>
        <AddPlayerModal team={state?.selectedTeam} handleClose={handleCloseAddPlayerModal} isOpen={state.openAddPlayerModal}/>
    </Box>
  ;
}

export default App;
