export type PlayerEntity = {
    id: number;
    name: string;
    lastname: string;
    position: string;
    number: number;
    is_captain: boolean;
}