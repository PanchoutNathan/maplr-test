import {PlayerEntity} from "./player.entity";

export type TeamEntity = {
    id: number;
    coach: string;
    year: number;
    players?: PlayerEntity[];
}