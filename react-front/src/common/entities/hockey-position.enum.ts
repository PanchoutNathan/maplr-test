export enum HockeyPositionEnum {
    CENTER = 'Center',
    DEFENSE_MAN = 'Defenseman',
    GOALIE = 'Goalie',
    LEFT_WING = 'Left Wing',
    RIGHT_WING = 'Right Wing',
}