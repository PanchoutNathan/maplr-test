import * as React from "react";
import * as Yup from "yup";
import {FunctionComponent} from "react";
import {
    Button,
    Checkbox,
    Dialog, DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle, FormControl,
    FormControlLabel,
    Grid, InputLabel, MenuItem, Select,
    TextField
} from "@mui/material";
import {useFormik} from "formik";
import {HockeyPositionEnum} from "../common/entities/hockey-position.enum";
import http from "../services/http/http.service";
import {AxiosError} from "axios";
import {TeamEntity} from "../common/entities/team.entity";

interface AddPlayerModalProps {
    isOpen: boolean;
    team: TeamEntity | null;
    handleClose: () => void;
}

export const AddPlayerModal: FunctionComponent<AddPlayerModalProps> = ({...props}) => {



    const formik = useFormik({
        initialValues: {
            name: '',
            lastname: '',
            number: '',
            position: HockeyPositionEnum.DEFENSE_MAN,
            is_captain: false,
        },
        validationSchema: Yup.object({
            name: Yup.string().required('Entrez le nom du joueur'),
            lastname: Yup.string().required('Entre le prénom du joueur'),
            number: Yup.number().typeError('Veuillez entez un nombre').required('Entrez le numéro du joueur'),
            position: Yup.string(),
            is_captain: Yup.boolean(),
        }),
        onSubmit: values => {
            const postData: any = values;
            postData.number = +postData.number;
            http.post(`/teams/${props.team?.year}`, postData).then((response) => {
                handleClose();
            }).catch((error: AxiosError) => {
                console.log(error);
            })
        }
    });

    const handleClose = (): void => {
        formik.resetForm();
        props.handleClose();
    }

    return <Dialog open={props.isOpen} onClose={handleClose}>
        <DialogTitle>Ajouter un joueur</DialogTitle>
        <DialogContent>
            <DialogContentText>
                Veuillez remplir tout les champs afin d'ajouter un nouveau joueur dans l'équipe de {props?.team?.year}
            </DialogContentText>
            <Grid container marginTop={1} rowSpacing={2} columnSpacing={4}>
                <Grid item xs={12} md={6}>
                    <TextField
                        autoFocus
                        error={formik.touched.name != null && formik.errors.name != null}
                        helperText={formik.errors.name}
                        {...formik.getFieldProps('name')}
                        label="Nom"
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        {...formik.getFieldProps('lastname')}
                        label="Prénom"
                        error={formik.touched.lastname != null && formik.errors.lastname != null}
                        helperText={formik.errors.lastname}
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        {...formik.getFieldProps('number')}
                        label="Numéro"
                        error={formik.touched.number != null && formik.errors.number != null}
                        helperText={formik.errors.number}
                        fullWidth
                        variant="standard"
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormControl error={formik.touched.position != null && formik.errors.position != null} variant={"standard"} fullWidth>
                        <InputLabel id="position-label">Position</InputLabel>
                        <Select
                            fullWidth
                            labelId="position-label"
                            label="Position"
                            {...formik.getFieldProps('position')}

                        >
                            {Object.values(HockeyPositionEnum).map((position) => {
                                return <MenuItem key={position} value={position}>{position}</MenuItem>

                            })}
                        </Select>
                    </FormControl>

                </Grid>
                <Grid item xs={12} md={6}>
                    <FormControlLabel control={<Checkbox  {...formik.getFieldProps('is_captain')}  />} label="Capitaine ? " />
                </Grid>
            </Grid>
            <DialogActions>
                <Button onClick={formik.submitForm}>Ajouter</Button>
            </DialogActions>
        </DialogContent>
    </Dialog>
}
